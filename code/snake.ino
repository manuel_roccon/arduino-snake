//Arduino Snake Revival V 1.1 Final
//Author Roccon Manuel

#include <binary.h>
#include <avr/io.h>
#include <avr/wdt.h>

#define Reset_AVR() wdt_enable(WDTO_30MS); while(1) {}
const int latchPin = 8;//Pin connected to ST_CP of 74HC595
const int clockPin = 12;//Pin connected to SH_CP of 74HC595
const int dataPin = 11; //Pin connected to DS of 74HC595
const int cost[] = {128, 64, 32, 16, 8, 4, 2, 1};
const int su = 0;   //direction up flag
const int giu = 1;  //direction down flag
const int sx = 2;   //direction left flag
const int dx = 3;   //direction right flag
const int SuPin = 4;    // Up switch
const int GiuPin = 5;   // Down switch
const int SxPin = 6;    // Left switch
const int DxPin = 7;    // Right switch

unsigned long previousMillis = 0;        // will store last time LED was updated
int data[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
int x[50] = {6, 7, 8}; //X
int y[50] = {4, 4, 4}; //Y
int contatore = 3;
int dir = 2;      //direction
int pres = 0;     //press flag
long interval = 1000;           // interval to shift
int NewX; //X of new point to shift
int NewY; //Y of new point to shift
int Target = 0; //If snake target the new point
int TargetX;    //Target X point
int TargetY;    //Target Y point

void setup ()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  Serial.begin(9600);      // open the serial port at 9600 bps:
  pinMode(SuPin, INPUT);      // declare pushbutton as input
  pinMode(GiuPin, INPUT);    // declare pushbutton as input
  pinMode(SxPin, INPUT);    // declare pushbutton as input
  pinMode(DxPin, INPUT);    // declare pushbutton as input
  delay(1000); //wait for a microsecond
}
void loop()
{
  unsigned long currentMillis = millis();
  // put your main code here, to run repeatedly:
  if ( digitalRead(SuPin) == HIGH && pres == 0 && dir != giu) {
    dir = su;
    pres = 1;
  }
  if ( digitalRead(GiuPin) == HIGH && pres == 0 && dir != su) {
    dir = 1;
    pres = giu;
  }
  if ( digitalRead(SxPin) == HIGH && pres == 0 && dir != dx) {
    dir = sx;
    pres = 1;
  }
  if ( digitalRead(DxPin) == HIGH && pres == 0 && dir != sx) {
    dir = dx;
    pres = 1;
  }
  if (currentMillis - previousMillis >= interval) {
    //create next point
    newpunto();
    //if the new point is in the matrix or not in the array, shift all the point
    if ((NewX >= 0 && NewX <= 7 && NewY >= 0 && NewY <= 7) && arrayIncludeElement(NewX, NewY) == false) {
      shift();
      interval = interval - 5;
    } else {
      //reset game
      Reset_AVR();
    }
    //create new target
    if (Target == 1) {
      newtarget();
      Target = 0;
    }
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    pres = 0;
  }
  //clear led
  clear_led();
  //add target to the matrix array
  add_target();
  //add snake point to the matrix
  add_snake();
  //print the matrix array in led matrix
  stampa();
}

void stampa() {
  int dat = 0x01;//0000 0001 ,control the cols
  for (int num = 0; num < 8; num++)
  {
    delay(1); //wait for a microsecond
    shiftOut(dataPin, clockPin, MSBFIRST, ~data[num]); //invert the data array ,control rows of dot matrix
    shiftOut(dataPin, clockPin, MSBFIRST, ~dat); // invert the dat ,control cols
    //return the latch pin high to signal chip that it
    //no longer needs to listen for information
    digitalWrite(latchPin, HIGH); //pull the latchPin to save the data
    delay(1); //wait for a microsecond
    digitalWrite(latchPin, LOW); //ground latchPin and hold low for as long as you are transmitting
    dat = dat << 1; //left shift 1 bit,turn on the cols one by one
  }
}

void newpunto() {
  if (dir == su && x[0] <= 7) {
    NewX = x[0];
    NewY = y[0] - 1;
  }
  if (dir == giu && x[0] >= 0) {
    NewX = x[0];
    NewY = y[0] + 1;
  }
  if (dir == sx && y[0] <= 7) {
    NewY = y[0];
    NewX = x[0] - 1;
  }
  if (dir == dx && y[0] >= 0) {
    NewY = y[0] ;
    NewX = x[0] + 1;
  }
}

void shift() {
  //check if the target x y is the same of last point of the snake
  if (x[0] == TargetX && y[0] == TargetY) {
    Target = 1;
  }
  //shift the snake of one position
  for (int num = contatore; num > 0; num--)
  {
    x[num] = x[num - 1];
    y[num] = y[num - 1];
  }
  x[0] = NewX;
  y[0] = NewY;
  //If target add a new point
  if (Target == 1) {
    contatore = contatore + 1;
  }

}

void newtarget() {
  //create a new random point
  TargetX = random(0, 7);
  TargetY = random(0, 7);
  for (int num = 0; num < contatore; num++) {
    if (x[num] == TargetX && y[num] == TargetY) {
      num = 0;
      TargetX = random(0, 7);
      TargetY = random(0, 7);
    }
  }
}

boolean arrayIncludeElement(int X, int Y) {
  for (int i = 0; i < contatore; i++) {
    if (x[i] == X && y[i] == Y) {
      return true;
    }
  }
  return false;
}

void clear_led() {
  //clear che array of led
  for (int num = 0; num < 8; num++)
  {
    data[num] = 0xFF;
  }
}

void add_target() {
  //add target to led matrix array
  if (!arrayIncludeElement(TargetX, TargetY)) {
    data[TargetX] = data[TargetX] - cost[TargetY];
  } else {
    delay(15); //wait for a microsecond
  }
}

void add_snake() {
  //add snake point to led matrix array
  for (int num = 0; num < contatore; num++) {
    data[x[num]] = data[x[num]] - cost[y[num]];
  }
}
